// difficulty variables used in skill checks
VAR in_intro = true

VAR candidatesFound = 0
VAR candidateName0 = "a"
VAR candidateName1 = "b"
VAR candidateName2 = "c"

VAR dependents = 0

// DEBUG mode adds a few shortcuts - remember to set to false in release!
VAR DEBUG = true
{DEBUG:
	IN DEBUG MODE!
	*	[work]	-> work
	*	[leisure] -> leisure
- else:
	// First diversion: where do we begin?
 -> starting_game
}

-> starting_game

 === function dec(ref x)
 	~ x = x - 1

 === function inc(ref x)
 	~ x = x + 1
 	
=== starting_game === 

* Clock In 
    ~ in_intro = false
    -> work
* Clock Out -> END

=== work ===
{!The choice is yours}

{&welcome to the family|please click the work button|click work button|you know what's up|we aren't paying you to do nothing|please hurry up with your assignment|we're counting on you|quarterly reviews are coming up!|is this the best you can do?|yeah I'm gonna need you to do a little more|I'm expecting a little more from you|you don't want to get written up right?|you're on the short list for promotion!|}
+ [work] -> work

=== leisure ===
{!The choice is yours}

+ [rest] -> rest
+ [study] -> study
+ [find date] -> find_date
+ {candidatesFound > 0} [date {candidateName0}] -> dating1  
+ {candidatesFound > 1} [date {candidateName1}] -> dating2 
+ {candidatesFound > 2} [date {candidateName2}] -> dating3 
+ [parenting] -> parenting

=== find_date ===
~ temp dice_roll = RANDOM(1, 5)
{dice_roll == 1: hooray a match! -> lucky} 
{&no luck|dang|struck out again|not interested|ghosted|getting strung along|rejected|no good candidates|lazy messaging|too tired atm|too boring|not a looker|my parents wouldn't approve|out of my league|looks like a deadbeat|no ambition|politically incorrect|too culturally different|wrong height} -> leisure

=== lucky ===
~ inc(candidatesFound)
+ [lucky!] They seem nice
-> leisure

=== rest ===
you feel better -> leisure

=== study ===
you get slightly better at your job
-> leisure

=== dating1 ===
dating time with {candidateName0} // rich guy who sucks
 -> leisure

=== dating2 ===
dating time with {candidateName1} // poor guy who is great
 -> leisure

=== dating3 ===
dating time with {candidateName2} // average guy
 -> leisure

=== parenting ===
you have {dependents} dependent(s)
+ [have baby] -> leisure
+ {dependents > 0} [feed baby] the baby likes it -> leisure 

=== dead ===
game over
-> END

