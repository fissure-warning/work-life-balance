using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Drawing;
using System.Net.Mime;
using System.Numerics;
using Ink.Runtime;
using MoreMountains.Tools;
using UnityEngine.SceneManagement;
using Color = UnityEngine.Color;
using Vector3 = UnityEngine.Vector3;

namespace nitroclay {
	public class Game : MonoBehaviour {
		[SerializeField] private TextAsset inkleStory = null;
		Story story;
		public static event Action<Story> OnCreateStory;

		[SerializeField] private RectTransform TimeTracker;
		[SerializeField] private Transform topTextGroup;
		[SerializeField] private Transform topChoiceGroup;
		[SerializeField] private Transform bottomTextGroup;
		[SerializeField] private Transform bottomChoiceGroup;
		[SerializeField] private Text startInfoText;

		[SerializeField] private MMProgressBar moneyBar;
		[SerializeField] private MMProgressBar joyBar;

		// UI Prefabs
		[SerializeField] private Text textPrefab = null;
		[SerializeField] private MMTouchButton buttonPrefab = null;

		// Audio Clips
		public AudioClip instrumental;
		public AudioClip vocal;
		[SerializeField] private AudioCrossFade _audioCrossFade;

		// options
		public float buttonDelay = 1f;

		private void clearChildren(Transform clearMe) {
			int childCount = clearMe.childCount;
			for (int i = childCount - 1; i >= 0; --i) {
				GameObject.Destroy(clearMe.GetChild(i).gameObject);
			}
		}
		private void ClearText() {
			Debug.Log("clearing text");
			clearChildren(topTextGroup);
			clearChildren(bottomTextGroup);
		}

		public void SetInfoText(int idx) {
			switch (idx) {
				case 0:
					startInfoText.text = "Instructions: \n \n" +
					                     "The game ends when your money or joy reaches zero \n\n" +
					                     "Time is split between work and leisure, indicated by the far left timer \n\n" +
					                     "if you get enough money you can feed a baby";
					break;
				case 1:
					startInfoText.text = "~ Duality ~\n\n" +
					                     "Marx posits that in the capitalist mode of production it is workers who produce commodities and that is the beating heart of the system.\n" +
					                     "but social reproduction theory asks the question \n" +
					                     "if workers produce commodities \n" +
					                     "who produces the worker? \n\n" +
					                     "- Tithi Bhattacharya";
					break;
				case 2:
					startInfoText.text = "created by nitroclay \n" +
					                     "for global gam jam 2022 with PIGSquad in Portland OR \n\n"
					                     +
					                     "Made in Unity \n" +
					                     "with plugins/tooling from\n" +
					                     "inkle ~ a game narrative markup language\n" +
					                     "FEEL ~ a UI library for visual flair \n" +
										 "Kenny assets ~ the time, dollar, heart icons \n" +
					                     "Music from the album \"Human\" provided royalty free by The Hunts \n" +
					                     "webgl browser template from https://seansleblanc.itch.io";
					break;
			}
		}

		private void ClearChoices() {
			Debug.Log("clearing choices");
			clearChildren(topChoiceGroup);
			clearChildren(bottomChoiceGroup);
		}

		IEnumerator ClearButtonsDelayed(bool top, float delay) {
			yield return new WaitForSeconds(delay);
			ClearChoices();
			RefreshView(_gameStage != GS.leisure && top, story);
		}

		void AppendTextView(bool top, String text, Color color) {
			Text storyText = Instantiate(textPrefab) as Text;
			storyText.text = text;
			storyText.color = color;

			storyText.transform.SetParent(top ? topTextGroup : bottomTextGroup, false);
			if (!top) {
				storyText.transform.SetAsFirstSibling();
			}
		}

		MMTouchButton AppendChoiceView(Transform parent, String text) {
			MMTouchButton choice = Instantiate(buttonPrefab) as MMTouchButton;
			choice.transform.SetParent(parent, false);
			Text choiceText = choice.GetComponentInChildren<Text>();
			choiceText.text = text;
			return choice;
		}

		void RefreshView(bool top, Story story) {
			while (story.canContinue) {
				AppendTextView(top, story.Continue().Trim(), top ? Color.white : Color.black);
			}

			// Display all the choices, if there are any!
			// StartCoroutine(ClearExistingChoices(3f));
			if (story.currentChoices.Count > 0) {
				for (int i = 0; i < story.currentChoices.Count; i++) {
					Choice choice = story.currentChoices[i];
					MMTouchButton button = AppendChoiceView(top ? topChoiceGroup : bottomChoiceGroup, choice.text.Trim());
					button.ButtonPressedFirstTime.AddListener(delegate { OnClickChoiceButton(top, story, button, choice); });
				}
			}
			else {
				Debug.Log("no choices, presenting quit game");
				MMTouchButton button = AppendChoiceView(top ? topChoiceGroup : bottomChoiceGroup, "Restart");

				button.ButtonReleased.AddListener(delegate {
					Scene scene = SceneManager.GetActiveScene();
					SceneManager.LoadScene(scene.name);
				});

				MMTouchButton quit = AppendChoiceView(top ? topChoiceGroup : bottomChoiceGroup, "Quit Game");
				quit.ButtonReleased.AddListener(delegate {
					Application.Quit();
				});
			}
		}

		private float lastTimeButtonClicked;
		public float noClickThreshold = 1f;

		private void updateStoryWithDateOutcome(bool wentWell) {
			if (wentWell) {
				AppendTextView(true, "the date went well", Color.cyan);
			}
			else {
				AppendTextView(true, "the date went poorly", Color.magenta);
				if (player.candidatesFound + 1 == (int)story.variablesState["candidatesFound"]) {
					AppendTextView(true, "this person has decided to stop dating you", Color.red);
				}
				story.variablesState["candidatesFound"] = player.candidatesFound;
			}
		}

		void OnClickChoiceButton(bool top, Story story, MMTouchButton button, Choice choice) {

			if (percentage > 0.95f || (percentage > 0.45f && percentage < 0.5f))
				return;
			if (lastTimeButtonClicked + noClickThreshold >= Time.time)
				return;

			Debug.Log("choice picked: " + choice.text + "|" + choice.index);
			lastTimeButtonClicked = Time.time;

			switch (choice.text) {
				case "Quit Game":
					Application.Quit();
					break;
				case "work":
					if (player.joy > 0.01f)
						player.worked();
					break;
				case "rest":
					if (player.money > 0.01f)
						player.rested();
					break;
				case "study":
					if (player.joy > 0.01f)
						player.studied();
					break;
				case "find date":
					player.wentLookingForDate();
					break;
				case "lucky!":
					player.foundDate((int)(story.variablesState["candidatesFound"]));
					story.variablesState["candidateName0"] = player.candidateNames[0];
					story.variablesState["candidateName1"] = player.candidateNames[1];
					story.variablesState["candidateName2"] = player.candidateNames[2];
					break;
				case "have baby":
					if (player.money > 0.8f && player.joy > 0.01f) {
						player.hadBaby();
						story.variablesState["dependents"] = player.dependents;
						AppendTextView(true, "congratulations! you've socially reproduced a future worker!", Color.green);
					}
					else {
						AppendTextView(true, "not enough money for a baby", Color.yellow);
					}
					break;
			}

			if (player.money > 0.01f && player.joy > 0.01f) {
				for (int i = 0; i < player.candidatesFound; i++) {
					if (choice.text == "date " + player.candidateNames[i]) {
						updateStoryWithDateOutcome(player.dated(i));
					}
				}
			}
			else {
				AppendTextView(true, top ? "too sad to work" : "too sad or broke", Color.yellow);
			}

			StartCoroutine(ClearButtonsDelayed(top, buttonDelay));
			if (choice.index < story.currentChoices.Count)
				story.ChooseChoiceIndex(choice.index);

		}

		private enum GS { start, work, leisure }
		private GS _gameStage;
		private Timer timer;
		public Player player; // set in the gameobject

		private void loadInkle(Story story) {
			Debug.Log("loading story");
			if (OnCreateStory != null) {
				OnCreateStory(story);
			}
		}

		void Awake() {
			timer = new Timer();
			_gameStage = GS.start;
			story = new Story(inkleStory.text);
			loadInkle(story);
			story.ChoosePathString("starting_game");
			RefreshView(true, story);
		}

		public float percentage;
		void Update() {
			if (_gameStage == GS.start) {

				moneyBar.SetBar01(player.money);
				joyBar.SetBar01(player.joy);

				if ((bool)story.variablesState["in_intro"] == false) {
					// game clock starts here
					ClearText();
					_gameStage = GS.leisure;
					_audioCrossFade.ChangeSong(instrumental);
					_audioCrossFade.taretVolume = 0.5f;
				}
			}
			else {
				// -- sync player info to the money and joy bar
				if (_gameStage == GS.work) {
					joyBar.UpdateBar01(player.joy);
				}
				else {
					joyBar.UpdateBar01(player.joy);
					moneyBar.UpdateBar01(player.money);
				}

				// -- check game over
				player.fedDependents();
				if (player.dependents > 0) {
					AppendTextView(false, "fed " + player.dependents + " children", Color.blue);
				}

				if (player.isDead() != "") {
					AppendTextView(true, player.isDead(), Color.red);
					story.ChoosePathString("dead");
				}

				// -- update timer position
				percentage = timer.percentagePlusPlus(Time.deltaTime);
				TimeTracker.localPosition = new Vector3(0, -600f * percentage + 300f, 0);

				// -- divert to new scene as time percentage dictates
				if (percentage <= 0.5f && _gameStage != GS.work) {
					ClearText();
					ClearChoices();
					story.ChoosePathString("work");
					_gameStage = GS.work;
					RefreshView(true, story);
					_audioCrossFade.ChangeSong(instrumental);
					_audioCrossFade.taretVolume = 0.5f;
				} else if (percentage > 0.5f && _gameStage == GS.work) {
					ClearText();
					ClearChoices();
					story.ChoosePathString("leisure");
					_gameStage = GS.leisure;
					RefreshView(false, story);
					_audioCrossFade.ChangeSong(vocal);
					_audioCrossFade.taretVolume = 1f;

				}
			}
		}
	}
}
