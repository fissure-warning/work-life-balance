using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace nitroclay {
    public class Player : MonoBehaviour {
        public float money;
        public float joy;

        public int dependents = 0;
        public int candidatesFound = 0;

        // gonna be hidden from the player
        public float workSkill = 0f;
        public float socialSkill = 0f;

        public float workSkillRate = 0.0001f;
        public float socialSkillRate = 0.01f;

        public String[] candidateNames;
        public float[] candidateCompatibility;
        public float[] candidateEarningRate;
        public float[] candidatePatience;

        void Awake() {
            candidateNames = new String[] {"a", "b", "c"};
            candidateCompatibility = new float[]{0f,0f,0f};
            candidateEarningRate = new float[] {0f, 0f, 0f};
            candidatePatience = new float[] {0f, 0f, 0f};
        }

        public void generateCandidate(int idx) {
            candidateNames[idx] = randomName();
            float compatibility = Random.value;
            candidateCompatibility[idx] = compatibility;
            candidateEarningRate[idx] = 1.0f - compatibility;
            candidatePatience[idx] = compatibility / 3.0f;
        }

        public void wentLookingForDate() {
            money -= 0.01f;
            joy -= 0.01f;
        }

        public void foundDate(int candidates) {
            joy += 0.1f;

            socialSkill += socialSkillRate;

            candidatesFound = Math.Min(candidates, 3);

            candidateNames[2] = candidateNames[1];
            candidateCompatibility[2] = candidateCompatibility[1];
            candidatePatience[2] = candidatePatience[1];

            candidateNames[1] = candidateNames[0];
            candidateCompatibility[1] = candidateCompatibility[0];
            candidatePatience[1] = candidatePatience[0];

            generateCandidate(0);
        }

        public string isDead() {
            if (money <= 0f) {
                if (dependents > 0) {
                    return "ran out of money, dependents separated";
                }
                return "ran out of money";
            } else if (joy <= 0f) {
                return "fell into a depression";
            }

            return "";
        }

        public float worked() {
            money += 0.0125f + workSkill;
            joy -= 0.01f;

            return money;
        }

        public float rested() {
            money -= 0.01f;
            joy += 0.0125f;
            return joy;
        }

        public float studied() {
            joy -= 0.01f;
            workSkill += workSkillRate;
            return workSkill;
        }

        public bool hadBaby() {
            money -= 0.7f;
            dependents++;
            return true;
        }

        public void fedDependents() {
            money -= dependents * 0.05f; // babies cost 5 dollars a day.
        }

        public int removeCandidate(int idx) {
            joy -= 0.1f;

            if (idx == 0) {
                candidateNames[0] = candidateNames[1];
                candidateCompatibility[0] = candidateCompatibility[1];
                candidatePatience[0] = candidatePatience[1];

                candidateNames[1] = candidateNames[2];
                candidateCompatibility[1] = candidateCompatibility[2];
                candidatePatience[1] = candidatePatience[2];
            } else if (idx == 1) {
                candidateNames[1] = candidateNames[2];
                candidateCompatibility[1] = candidateCompatibility[2];
                candidatePatience[1] = candidatePatience[2];
            } else if (idx == 2) {

            }

            candidatesFound -= 1;
            return candidatesFound;
        }

        public bool dated(int idx) {
            float successProbability = 0.25f + socialSkill;
            money -= 0.01f;
            socialSkill += socialSkillRate;

            if (Random.value < successProbability) {
                candidateCompatibility[idx] += 0.1f;
                joy += 0.1f + socialSkill*0.01f;
                return true;
            }
            else {
                candidateCompatibility[idx] -= 0.1f + socialSkill;
                joy -= 0.02f + socialSkill*0.01f;

                if (candidateCompatibility[idx] < 0) {
                    removeCandidate(idx);
                }
                return false;
            }
        }

        public static String[] randomNames = new String[] {
            "Alex", "Avery", "Adrian", "Amari", "Ari",
            "Blake", "Bailey", "Beckham","Blair","Bryce",
            "Camden","Carey","Cassidy", "Collins", "Cameron",
            "Dakota","Dallas","Dane","Darryl",
            "Ezra", "Ember", "Emerson", "Emory", "Elliot",
            "Florian",
            "Gabriel", "Gale", "Grey",
            "Harper", "Harley",
            "Indigo",
            "Jamie", "Jayden", "Jesse","Jordan", "Juniper",
            "Kaden", "Kai", "Kelly", "Kerry",
            "Lee", "Lennox", "Linden", "Lumi",
            "Madison", "Marley", "Micah", "Mina", "Morgan",
            "Nico", "Noel",
            "Ollie", "Oakley",
            "Parker", "Palmer", "Pat", "Piper",
            "Quinn",
            "Reed", "Reese", "Remy", "Riley", "River", "Roan", "Robin", "Ryder",
            "Sage", "Salem", "Sam", "Scout", "Shawn", "Skylar", "Sterling", "Sunny", "Sydney",
            "Taylor", "Tracy",
            "Val", "Vesper",
            "West", "Winter", "Wynne"};

        public string randomName() {
            int idx = Random.Range(0, randomNames.Length);
            return randomNames[idx];
        }
    }
}