using UnityEngine;

namespace nitroclay {
    public class Timer {
        private int _fixedUpdatesPerHour = 100;
        private int _hoursPerDay = 24;
        private float hour = 0;
        private float minute = 0;

        public float percentagePlusPlus(float secondsDelta) {
            minute += Time.deltaTime * 50f;
            if (minute > _fixedUpdatesPerHour) {
                hour++;
                minute = 0f;
            }
            if (hour >= _hoursPerDay) {
                hour = 0f;
            }
            return (hour / _hoursPerDay); // + (0.02f * (minute / _fixedUpdatesPerHour));
        }

        public void goToWorkTime() {
            hour = 0;
            minute = 0;
        }

        public void goHomeTime() {
            hour = 13;
            minute = 0;
        }

    }
}